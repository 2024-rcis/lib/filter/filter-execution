package de.ubt.ai4.petter.recpro.lib.filter.filterexecution.creator.bpmElementInstance;


import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.BpmElementInstance;
import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.Task;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.bpmElementInstance.FilterBpmElementInstanceInstance;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.bpmElementInstance.FilterBpmElementInstanceState;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class FilterBpmElementInstanceInstanceCreator {
    private FilterBpmElementInstanceInstance create(BpmElementInstance elementInstance) {
        FilterBpmElementInstanceInstance instance = new FilterBpmElementInstanceInstance();
        instance.setId(null);
        instance.setState(FilterBpmElementInstanceState.NEUTRAL);
        if (elementInstance instanceof Task) {
            instance.setRecproElementInstanceId(((Task) elementInstance).getProcessInstanceId());
        }
        return instance;
    }

    public List<FilterBpmElementInstanceInstance> create(List<Task> tasks) {
        List<FilterBpmElementInstanceInstance> result = tasks.stream().map(this::create).toList();
        return result.stream()
                .collect(Collectors.toMap(FilterBpmElementInstanceInstance::getRecproElementInstanceId, obj -> obj, (existing, replacement) -> existing))
                .values().stream().toList();
    }
}
