package de.ubt.ai4.petter.recpro.lib.filter.filterexecution.creator.filter;

import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.Filter;
import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.KnowledgeBasedFilter;
import de.ubt.ai4.petter.recpro.lib.filter.filterexecution.creator.attribute.FilterAttributeInstanceCreator;
import de.ubt.ai4.petter.recpro.lib.filter.filterexecution.creator.bpmElement.FilterBpmElementInstanceCreator;
import de.ubt.ai4.petter.recpro.lib.filter.filterexecution.creator.bpmElementInstance.FilterBpmElementInstanceInstanceCreator;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter.FilterInstance;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter.KnowledgeBasedFilterInstance;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class KnowledgeBasedFilterInstanceCreatorImpl extends FilterInstanceCreatorImpl {
    private FilterAttributeInstanceCreator attributeInstanceCreator;
    private FilterBpmElementInstanceCreator bpmElementInstanceCreator;
    private FilterBpmElementInstanceInstanceCreator bpmElementInstanceInstanceCreator;

    @Override
    public FilterInstance create(FilterInstance instance, Filter filter) {
        KnowledgeBasedFilter baseFilter = (KnowledgeBasedFilter) filter;
        KnowledgeBasedFilterInstance result = new KnowledgeBasedFilterInstance();
        this.initialize(result, baseFilter);
        result.setAttributes(attributeInstanceCreator.createAttributes(baseFilter.getAttributes()));
        result.setElements(bpmElementInstanceCreator.create(baseFilter.getBpmElements()));
        result.setElementInstances(bpmElementInstanceInstanceCreator.create(instance.getTasklist().getTasks()));
        return result;
    }
}
