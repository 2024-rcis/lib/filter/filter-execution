package de.ubt.ai4.petter.recpro.lib.filter.filterexecution.creator.filter;

import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.Filter;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter.CollaborativeFilterInstance;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter.FilterInstance;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CollaborativeFilterInstanceCreatorImpl extends FilterInstanceCreatorImpl {
    @Override
    public FilterInstance create(FilterInstance instance, Filter filter) {
        CollaborativeFilterInstance filterInstance = new CollaborativeFilterInstance();

        this.initialize(filterInstance, filter);

        return filterInstance;
    }
}
