package de.ubt.ai4.petter.recpro.lib.filter.filterexecution.creator.attribute;

import de.ubt.ai4.petter.recpro.lib.attribute.modeling.model.RecproAttribute;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.attribute.FilterAttributeInstance;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.attribute.FilterAttributeState;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.attribute.FilterAttributeType;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.attribute.FilterMetaAttributeInstance;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class FilterAttributeInstanceCreator {
    public List<FilterAttributeInstance> createAttributes(List<RecproAttribute> attributes) {
        return attributes.stream().map(this::createAttribute).toList();
    }

    private FilterAttributeInstance createAttribute(RecproAttribute attribute) {
        return switch (attribute.getAttributeType()) {
            case META -> this.createMetaAttributeInstance(attribute);
            default -> this.createBaseAttributeInstance(attribute);
        };
    }

    private FilterAttributeInstance createBaseAttributeInstance(RecproAttribute attribute) {
        FilterAttributeInstance instance = new FilterAttributeInstance();
        this.initializeAttribute(instance, attribute, FilterAttributeType.TEXT);
        return instance;
    }

    private FilterAttributeInstance createMetaAttributeInstance(RecproAttribute attribute) {
        FilterMetaAttributeInstance instance = new FilterMetaAttributeInstance();
        this.initializeAttribute(instance, attribute, FilterAttributeType.META);
        return instance;
    }
    private void initializeAttribute(FilterAttributeInstance instance, RecproAttribute attribute, FilterAttributeType filterAttributeType) {
        instance.setAttributeId(attribute.getId());
        instance.setFilterAttributeType(filterAttributeType);
        instance.setActive(false);
        instance.setState(FilterAttributeState.NEUTRAL);
    }
}
