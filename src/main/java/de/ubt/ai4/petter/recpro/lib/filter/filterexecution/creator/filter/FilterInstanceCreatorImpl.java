package de.ubt.ai4.petter.recpro.lib.filter.filterexecution.creator.filter;

import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.Filter;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter.FilterInstance;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.result.FilterResult;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class FilterInstanceCreatorImpl implements IFilterInstanceCreator {

    public void initialize(FilterInstance filterInstance, Filter filter) {
        filterInstance.setFilterId(filter.getId());
        filterInstance.setConsiderProcessAttributes(true);
        filterInstance.setFilterType(filter.getFilterType());
        filterInstance.setCalledUrl(filter.getFilterUrl());
        filterInstance.setResult(new FilterResult());
    }

    @Override
    public FilterInstance create(FilterInstance instance, Filter filter) {
        FilterInstance filterInstance = new FilterInstance();

        this.initialize(filterInstance, filter);

        return filterInstance;
    }
}
